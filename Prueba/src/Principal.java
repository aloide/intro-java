public class Principal {


    public static void main (String arg[]){
        /*System.out.print("Hola mundo");*/

        float a = 3;
        int b = 4;

        double c = (a / b);

        //System.out.println(c);

        if( b > a){
            System.out.println("B es mayor que A");
        }else if(a == b){
            System.out.println(1);
        }else if(b == a) {
            System.out.println(2);
        }
        else{
            System.out.println("A es mayor que B");
        }

        switch (b){
            case 3:
                System.out.println("soy 3");
            case 4:
                System.out.println("soy 4");
                break;
            case 5:
                System.out.println("soy 5");
                break;
            default:
                System.out.println("soy default");
        }



        while(b < 10){
            System.out.println("soy un while");
            b++;
        }

        do{
            System.out.println("soy un do while");
            b++;
        }while(b < 10);

        // for i in range(10):
        // SSS: start stop step
        int i = 1;
        for(;;){
            System.out.println("soy un for");
            if(i < 10 ){
                i++;
                break;
            }
        }

        for (;;){
            System.out.println("soy un bloque");
            break;
        }


    }

}
